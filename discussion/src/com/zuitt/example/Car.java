package com.zuitt.example;

public class Car {

    // Access Modifier
    // These are used to restrict the scope of a class, constructor, variable, method, or data member

    // Four Types of Access Modifier
    // 1. Default (no keyword)
    // 2. Private (mas mahigpit compared to protected)
    // 3. Protected (data/class can be access when they are in the same ...)
    // 4. Public


    // Class Creation
    // Four Parts of Class Creation

    // 1. Properties - characteristics of an object; also known as "variable"
    // Example:
    private String name;
    private String brand;
    private int yearOfMake;
    private Driver driver;
    // ^ Makes additional component of a car

    // 2. Constructor - used to create or instantiate an object
        // Two Types of Constructors
        // A. Empty Constructor - creates object that doesn't have any arguments/parameters; Also referred as default constructor.
        // Example:
        public Car(){
            // If no value (empty), the result will be 0
            // To set a default value upon instantiation
            this.yearOfMake = 2000;

            // Whenever a new car is created, it will have a driver named "Alejandro" (default value of driver)
            this.driver = new Driver("Alejandro");
        };

        // B. Parameterized Constructor - creates an object with arguments or parameters.
        // Example:
        public Car(String name, String brand, int yearOfMake){
            this.name = name;
            this.brand = brand;
            this.yearOfMake = yearOfMake;
            this.driver = new Driver("Alejandro");
        }

    // 3. Getters and Setters - get(update and add) and set(receive??) the value of each property of the object
        // Getters - retrieves the value of instantiated object
        // Syntax / Example:
        public String getName(){
            return this.name;
        }

        public String getBrand(){
            return this.brand;
        }

        public int getYearOfMake(){
            return  this.yearOfMake;
        }

        public String getDriverName(){
            return this.driver.getName();
        }


        // Setters - used to change the default value of an instantiated object.
        // Syntax / Example:

        public void setName(String name){
            this.name = name;
        }

        public void setBrand(String brand){
            this.brand = brand;
        }

        public void setYearOfMake(int yearOfMake){
            // Can also be modified to add validation
            if(yearOfMake <= 2022){
                this.yearOfMake = yearOfMake;
            }
        }

        public void setDriver(String driver){
            // This will invoke the setName() method of the Driver class
            this.driver.setName(driver);
        }

    // 4. Methods - functions that an object can perform (actions); This is optional.
    // Example:

    public void drive(){
            System.out.println("This car is running. Vrroom Vroom");
    }


}
