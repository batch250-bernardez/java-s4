package com.zuitt.example;
// Child Class
// "Extends" keyword - used to inherit the properties and methods of the parent class

public class Dog extends Animal{

    // Properties
    private String breed;

    // Constructor
    public Dog(){
        // "Super" method - to have a direct access with the original constructor coming from out parent class.
        super();
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }

    // Getter and Setter
    // Getter
    public String getBreed(){
        return this.breed;
    }

    // Setter
    public void setBreed(String breed){
        this.breed = breed;
    }

    // Method
    public  void speak(){
        System.out.println("Woof, woof");
    }

    public void call(){
        super.call();
        System.out.println("Hi my name is " + this.name + ", I am a dog");
    }

}
