package com.zuitt.example;

public class Driver {

    // Properties or Variables
    private String name;


    // Template for Constructor
    public Driver(){};

    public Driver(String name){
     this.name = name;
    }


    // Getter and Setter (Manipulate values)

    // Getter
    public String getName(){
        return this.name;
    }

    // Setter
    public void setName(String name){
        this.name = name;
    }

}
